#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

/**
       * readFromFile function takes 3 parameters.
       * @param fileName a string argument.
       * @param numbers a float vector reference. 
       * @param count an integer reference, 
       * 
       * Vector passed by reference to not create a new vector every time the function called
       * 
       * count passed by referenece because when it's defined in the function it should be available in the main scope
       */

void readFromFile(string fileName, vector<float> &numbers, int &count)
{
    try
    {
        //! opens the file
        ifstream file(fileName);
        //! takes the first input from file and assigns it to the count
        file >> count;
        float number;

        if (file.is_open())
        {
            //! while file has data pushes them into the vector called numbers
            while (file >> number)
            {
                numbers.push_back(number);
            }

            file.close();
        }

        //! checks whether the first line has the correct data about the numbers in file
        if (count != numbers.size() || count == 0)
        {
            throw -1;
        }
    }
    catch (int x)
    {
        cerr << "The number of provided numbers doesn't match with the first line or file couldn't open" << endl;
        exit(EXIT_FAILURE);
    }
}

/**
       * calculateSum function takes 2 parameters and returns a float value.
       * @param count an integer, 
       * @param numbers a float vector reference. 
       * 
       * iterates through the vector and sums every element
       */

float calculateSum(int count, vector<float> &numbers)
{
    int sum = 0;
    for (int i = 0; i < count; i++)
    {
        sum += numbers[i];
    }
    return sum;
}

/**
       * calculateProduct function takes 2 parameters and returns a float value.
       * @param count an integer, 
       * @param numbers a float vector reference. 
       * 
       * calculates product of numbers in the vector by iterating through elements
       */

float calculateProduct(int count, vector<float> &numbers)
{
    int result = 1;
    for (int i = 0; i < count; i++)
    {
        result *= numbers[i];
    }
    return result;
}

/**
       * findMin function takes 2 parameters and returns a float value.
       * @param count an integer, 
       * @param numbers a float vector reference. 
       * 
       * determines the smallest element in the vector and returns that value
       */
float findMin(int count, vector<float> &numbers)
{
    //! assumes that the first element of the vector is the smallest one
    int minNumber = numbers[0];
    for (int i = 1; i < count; i++)
    {
        //! if there is another element which is smaller, assigns its value to minNumber
        if (numbers[i] < minNumber)
        {
            minNumber = numbers[i];
        }
    }
    return minNumber;
}

//! main function runs first
int main()
{
    int count;                                 //!< count variable is for keep track of how many elements in the file
    vector<float> numbers;                     //!< vector holds the numbers
    readFromFile("input.txt", numbers, count); //!< function for reading from the file
    //! outputs
    cout << "Sum is " << calculateSum(count, numbers) << endl;
    cout << "Product is " << calculateProduct(count, numbers) << endl;
    cout << "Average is " << calculateSum(count, numbers) / count << endl;
    cout << "Smallest is " << findMin(count, numbers) << endl;

    return 0;
}