#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
    int n,q,s=0;
    cin>>n>>q;
    int* array[n];

    while(n--) {
        int x;
        cin>>x;
        array[s] = new int[x];

        for(int i=0; i<x; i++){
            cin>>array[s][i];
        }

        s++;
    }

    while(q--) {
        int a,b;
        cin>>a>>b;

        cout << array[a][b] << endl;
    }

    return 0;
}
